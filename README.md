# Sass Boilerplate
[![NPM version][npm-image]][npm-url]
[![stability][stability-image]][stability-url]
## Install

With `yarn`

```bash
yarn add @thmsdmcrt_/sass-boilerplate
```

## How To

In your main sass file, import the sass-boilerplate

```scss
// main.scss
@import 'path/to/node_modules/@thmsdmcrt_/sass-boilerplate/scss/main';
```

# Helpers

## Easing

Based on [Ceaser](http://matthewlein.com/ceaser/)

```scss
$default: cubic-bezier(0.25, 0.1, 0.25, 1);
$linear: cubic-bezier(0.25, 0.25, 0.75, 0.75);
$easeIn: cubic-bezier(0.42, 0, 1, 1);
$easeOut: cubic-bezier(0, 0, 0.58, 1);

$easeInOut: cubic-bezier(0.42, 0, 0.58, 1);
$easeInQuad: cubic-bezier(0.55, 0.085, 0.68, 0.53);
$easeInCubic: cubic-bezier(0.55, 0.055, 0.675, 0.19);
$easeInQuart: cubic-bezier(0.895, 0.03, 0.685, 0.22);
$easeInQuint: cubic-bezier(0.755, 0.05, 0.855, 0.06);
$easeInSine: cubic-bezier(0.47, 0, 0.745, 0.715);
$easeInExpo: cubic-bezier(0.95, 0.05, 0.795, 0.035);
$easeInCirc: cubic-bezier(0.6, 0.04, 0.98, 0.335);
$easeInBack: cubic-bezier(0.6, -0.28, 0.735, 0.045);

$easeOutQuad: cubic-bezier(0.25, 0.46, 0.45, 0.94);
$easeOutCubic: cubic-bezier(0.215, 0.61, 0.355, 1);
$easeOutQuart: cubic-bezier(0.165, 0.84, 0.44, 1);
$easeOutQuint: cubic-bezier(0.23, 1, 0.32, 1);
$easeOutSine: cubic-bezier(0.39, 0.575, 0.565, 1);
$easeOutExpo: cubic-bezier(0.19, 1, 0.22, 1);
$easeOutCirc: cubic-bezier(0.075, 0.82, 0.165, 1);
$easeOutBack: cubic-bezier(0.175, 0.885, 0.32, 1.275);

// Inspired by material design
$easeOutSwift: cubic-bezier(0.55, 0, 0.1, 1);

$easeInOutQuad: cubic-bezier(0.455, 0.03, 0.515, 0.955);
$easeInOutCubic: cubic-bezier(0.645, 0.045, 0.355, 1);
$easeInOutQuart: cubic-bezier(0.77, 0, 0.175, 1);
$easeInOutQuint: cubic-bezier(0.86, 0, 0.07, 1);
$easeInOutSine: cubic-bezier(0.445, 0.05, 0.55, 0.95);
$easeInOutExpo: cubic-bezier(1, 0, 0, 1);
$easeInOutCirc: cubic-bezier(0.785, 0.135, 0.15, 0.86);
$easeInOutBack: cubic-bezier(0.68, -0.55, 0.265, 1.55);
```

## Mediaqueries

You can use the mediaqueries mixins, base on default breakpoint that you can overwrite.

```scss
/* Default Breakpoint */
$mq-min-width: 320px !default;
$mq-small-min: 640px !default;
$mq-medium-min: 960px !default;
$mq-large-min: 1200px !default;
$mq-xlarge-min: 1600px !default;
$mq-xxlarge-min: 1920px !default;

/* Mixin use case */
.my-class {
  @include mq-small {
    // your style
  }
}
```

## Fluid Font Size

Based on the article [Fluid Typography on Css Tricks](https://css-tricks.com/snippets/css/fluid-typography/), the sass-boilerplate provide a helpfull `@mixin` the create a linear interpolation between viewport size for the `font-size`.

```scss
$min_width: 320px;
$max_width: 1200px;
$min_font: 16px;
$max_font: 24px;

html {
  @include fluid-font-size($min_width, $max_width, $min_font, $max_font);
}
```

# Libraries

## Normalize.css

This boilerplate use normalize.css as a global reset for the vendors browser.

## Modularscale

This boilerplate include an helpful library to handle font size as a scaling system, [Modularscale](http://www.modularscale.com/).

Here is a small snippet to setting things up, in your `main.css` for exemple:

```scss
// {base} The refering font size, in css unit
// {ratio} the upscale/downscale ratio, as a number
$modularscale: (
  base: 1em,
  ratio: 1.5
);
```

Then, you could use it in your class declaration :

```scss
h4 {
  font-size: ms(3); // ms for the modularcale function
}
```
[stability-image]: https://img.shields.io/badge/stability-stable-brightgreen.svg?style=flat-square
[stability-url]: https://nodejs.org/api/documentation.html#documentation_stability_index
[npm-image]: https://img.shields.io/npm/v/@thmsdmcrt_/sass-boilerplate.svg?style=flat-square
[npm-url]: https://www.npmjs.com/package/@thmsdmcrt_/sass-boilerplate