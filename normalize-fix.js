/*
  This is a quick fix to rename normalize.css to _normalize.scss for @import ease. See https://github.com/sass/sass/issues/556#issuecomment-73439666
*/
var fs = require('fs')
if (fs.existsSync('./node_modules/normalize.css/normalize.css')) {
  fs.rename(
    './node_modules/normalize.css/normalize.css',
    './node_modules/normalize.css/_normalize.scss',
    err => {
      if (err) console.log('ERROR: ' + err)
    }
  )
}
